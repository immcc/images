# images

#### 介绍
个人使用的博客图床，主要配合 `PicGo` + `Typora` 使用上传文章对应的截图展示。

#### 使用说明

1. 下载 `PicGo`
2. 在 `PicGo` 中安装 `gitee-uploader`
3. 点开 图床设置 》 gitee，将项目参数进行配置。token 从 gitee 的设置中获取个人令牌。
